
Drupal.drupalit = function() {
  var dvdb = [];
  $('div.vote-wrap').each(function () {
    // Read in the path to the PHP handler	
    duri = $(this).children('a').attr('href');
    // Remove the title, so no tooltip will display
    $(this).removeAttr('title');
	// remove href link
	$(this).children('a').removeAttr('href');
    // Create an object with this uri. Because
    // we feed in the div as an argument, we'll be able
    // to attach events to this element.
    if (!dvdb[duri]) {
      dvdb[duri] = new Drupal.DVDB(this, duri);
    }
  });
}

/**
 * A Drupalit Vote DataBase object
 */
Drupal.DVDB = function(delt, duri, did) {
  var ddb = this;
  // By making the span element a property of this object,
  // we get the ability to attach behaviours to that element.
  this.delt = delt;
  this.duri = duri;
  this.did = $(delt).attr('id');
  $(delt).children('a').click(function() {
    $(delt).children('a').remove();
    // Ajax GET request for vote data
    $.ajax({
      type: "GET",
      url: ddb.duri,
      success: function (ddata) {
        // extract the dcid so we can change other elements for the same dcid
        var dcid = ddb.did.match(/[0-9]+$/);
        var dpid = 'vote-wrap-' + dcid;
        // update the points
        $('#' + dpid).html(ddata);
      },
      error: function (xmlhttp) {
        alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ ddb.duri);
      }
    });
  });
}

// Global killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.drupalit);
}