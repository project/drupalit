<?php

/**
 * @file drupalit.admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function drupalit_settings(&$form_state) {

  $form = array();

  $form['drupalit_node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#description' => t('Set the node types you want to activate drupalit on.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $array = array_keys(node_get_types('names'));

  for ($i = 0; $i < sizeof($array); $i++) {
    
    $form['drupalit_node_types']['drupalit_types'][$array[$i]] = array(
      '#type' => 'fieldset',
      '#title' => t($array[$i]),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['drupalit_node_types']['drupalit_types'][$array[$i]]['drupalit_'. $array[$i] .'_enabled'] = array(
      '#type' => 'radios',
      '#title' => t('Enable drupalit for this type'),
      '#default_value' => variable_get('drupalit_'. $array[$i] .'_enabled', '1'),
      '#options' => array( t('Yes'), t('No')),
    );
  }
  
  
  $form['drupalit_settings_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Drupalit settings for nodes'),
    '#description' => t('For more control over the drupalit widget placement it can be inserted directly in the theme, select "Manually" to do this.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['drupalit_settings_node']['drupalit_widget'] = array(
    '#type' => 'select',
    '#title' => t('Drupalit widget display'),
    '#default_value' => variable_get('drupalit_widget', 3),
    '#options' => array(0 => t('Manually'), 1 => t('Teaser view'), 2 => t('Full-page view'), 3 => t('Teasers and full-page view')),
    '#description' => t('Where to display the drupalit widget for nodes.'),
  );
  
  $form['drupalit_settings_node']['drupalit_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupalit interval'),
    '#default_value' => variable_get('drupalit_interval', 3600),
    '#description' => t('Time in seconds between votes a user can place for one node. 3600 equals 1 hour.'),
  );
  
  $form['drupalit_settings_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Drupalit roles vote power'),
    '#description' => t('Used if you want to have some user roles to have more "power" when voting.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $roles = user_roles(0, 0);
  
  foreach ($roles as $rid => $role) {
    $form['drupalit_settings_roles']['drupalit_vote_power_'. $rid] = array(
      '#type' => 'textfield',
      '#maxlenght' => 3,
      '#size' => 4,
      '#title' => $role,
      '#default_value' => variable_get('drupalit_vote_power_'. $rid, 1),
      '#description' => t('The votes number this role will cast when voting.'),
    );
  }
  
  $form['drupalit_settings_statistics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Drupalit statistics pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['drupalit_settings_statistics']['drupalit_articles_pager'] = array(
    '#type' => 'textfield',
    '#title' => t('Results for articles per page'),
    '#description' => 'Default value is 20 results per page.',
    '#default_value' => variable_get('drupalit_articles_pager', 20),
  );
  
  $form['drupalit_settings_statistics']['drupalit_users_pager'] = array(
    '#type' => 'textfield',
    '#title' => t('Results for users per page'),
    '#description' => 'Default value is 20 results per page.',
    '#default_value' => variable_get('drupalit_users_pager', 20),
  );

  return system_settings_form($form);
}